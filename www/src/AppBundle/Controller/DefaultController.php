<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

require_once __DIR__ . '/../../../app/config/parameters.php';

class DefaultController extends Controller
{
    /**
     * Renvoie un pong en Json et incr�mente le compteur de pings
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getPingAction(Request $request)
    {
        $path = $this->get('kernel')->getRootDir() . '/../src' . '/AppBundle/File/data.txt';
        $handle = fopen($path, 'r');
        $content = file_get_contents($path);
        if($content === false) {
            echo 'erreur';
        }
        fclose($handle);
        
        $val = intval($content)+1;
        
        $handle = fopen($path, 'w+');
        fwrite($handle, $val);
        fclose($handle);
        
        $result = ["message" => "pong"];
        
        $response = new Response();
        $response->setContent(json_encode($result));
        $response->headers->set('Content-Type', 'application/json');
        
        return $response;
    }
    
    /**
     * Renvoie la valeur du compte du ping en Json et envoie le d�but de l'application a Sentry
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getCountAction(Request $request)
    {
        $path = $this->get('kernel')->getRootDir() . '/../src' . '/AppBundle/File/data.txt';
        $handle = fopen($path, 'r');
        $counter = file_get_contents($path);
        if($counter === false) {
            echo 'erreur';
        }
        fclose($handle);
        
        
        $result = ["pingCount" => $counter];
        
        $response = new Response();
        $response->setContent(json_encode($result));
        $response->headers->set('Content-Type', 'application/json');
        
        return $response;
    }
    
    /**
     * Reset la valeur du compteur du ping et envoie la fin de l'application a Sentry
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getResetAction(Request $request)
    {
        $path = $this->get('kernel')->getRootDir() . '/../src' . '/AppBundle/File/data.txt';
        $handle = fopen($path, 'w+');
        fwrite($handle, '0');
        fclose($handle);
        
        $result = ["message" => "done"];
        
        $response = new Response();
        $response->setContent(json_encode($result));
        $response->headers->set('Content-Type', 'application/json');
        
        return $response;
    }
    
    /**
     * Envoie un message a Sentry
     * @param string $message
     */
    public static function captureMessage($message) {
        $sentryClient = new \Raven_Client(DNS_SENTRY);
        $sentryClient->install();
        
        $sentryClient->captureMessage($message);
    }
}
