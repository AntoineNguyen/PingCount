#!/usr/bin/env bash

server=$URL_SERVER
if [ $server != undefined ]
then
curl $server
echo -e "Test de la fonction ping\n"
curl --output logCurlPing.txt $server/ping
diff -s logCurlPing.txt testPing.txt
echo -e "Test de la fonction count\n"
curl --output logCurlCount.txt $server/count
diff -s logCurlCount.txt testCount1.txt
curl $server/reset
curl $server/ping
curl $server/ping
curl $server/ping
curl --output logCurlCount.txt $server/count
diff -s logCurlCount.txt testCount2.txt
fi
