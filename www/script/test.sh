#!/usr/bin/env bash
echo "sleep 5s\n"
sleep 5
echo "Test du serveur : $URL"
 
ip_server=$URL

# Condition permettant de tester le renvoie de la route /ping
ping=$(curl -s "$ip_server/ping"  | jq '.message' 2>/dev/null)
echo -e "Ping : $ping\n"
if [[ "$ping" == "\"pong\"" ]]  # Incrémente les compteurs si le serveur renvoie "pong"
then
	echo -e "$ip_server : Serveur fonctionnel\n"

	compteur1=`curl -s "$ip_server/count" | jq .pingCount`
	echo "Valeur compteur avant test : $compteur1"
	
	curl -s "$ip_server/ping" &>/dev/null
	
	compteur2=`curl -s "$ip_server/count" | jq .pingCount`
	echo "Valeur compteur après test : $compteur2"

# Quitte le script si le serveur ne renvoie pas "pong"
else 
	echo -e "Erreur de réponse serveur\n	
Sortie du script ... "
	exit 1
fi

# Compare la valeur des compteur et valide l'incrémentation ou non

if [[ "$compteur2" != "$compteur1" ]]
then
	echo "Le compteur a bien été incrémenté"
else
	echo "Le compteur n'a pas changé"
	exit 1
fi

exit 0
