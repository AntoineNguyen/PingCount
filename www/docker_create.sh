#!/bin/bash
. .env
echo $PATH_TO_PROJECT
docker rm -f pingcount
docker run -d -p 80:80 --name pingcount -v "$PATH_TO_PROJECT":/var/www/html php:5.6-apache
