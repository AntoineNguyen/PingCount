PingCount
=======

## DNS Sentry

Pour remplir le DNS, vous devez aller créer le fichier `app/config/parameters.php` avec l'exemple du fichier [app/config/parameters.sample.php](app/config/parameters.sample.php) (extrait ci-dessous) :

```php
<?php 
define('DNS_SENTRY', 'https://******@sentry.io/252409');
```

Vous devez remplacer le DNS par défaut par le votre.

## Application serveur via symfony/web-server-bundle

- Vous avez besoin d'avoir ces extensions : `php5-cli` (ou `php7-cli`), `php-xml`, `php-curl`.
- Vous avez besoin d'avoir [composer](https://getcomposer.org/download/) pour le gestionnaire de dépendances et de faire `composer install` à la racine du projet.
- Vous devez lancez le script `server_run.sh` pour lancer le serveur.
- Utiliser les adresses webservices pour [ping](http://localhost:8000/ping), [count](http://localhost:8000/count) et [reset](http://localhost:8000/reset) (qui reset le compteur)
- Lorsque l'on démarre l'application, cela envoie un message "Lancement de l'application" dans Sentry.
- Lorsque l'on quitte l'application, cela envoie un message "Fermeture de l'application" dans Sentry.

## Docker

- Ajouter le ficher `.env` selon le modèle du fichier `.env-sample` avec le chemin de vers le projet PingCount.
- /!\ DOCKER /!\ Lancer le script sh `docker_create.sh` pour créer le container basé sur l'image officiel `php:5.6-apache`.
- Pour utiliser [composer](https://getcomposer.org/) veuillez rentrer dans le container avec `docker exec -it pingcount bash` et de faire `composer install` à la racine du projet.
- Vous devez lancez le script `server_run.sh` pour lancer le serveur
- Utiliser les adresses webservices pour [ping](http://localhost/web/app.php/ping), [count](http://localhost/web/app.php/count) et [reset](http://localhost/web/app.php/reset) (qui reset le compteur)
- Lorsque l'on démarre l'application, cela envoie un message "Lancement de l'application" dans Sentry.
- Lorsque l'on quitte l'application, cela envoie un message "Fermeture de l'application" dans Sentry.

## Tests unitaires

- Lancer `./script/check.sh` pour voir les tests unitaires.
- 1er test : Envoi de /ping et check du message reçu (`{"message":"pong"}`)
- 2ème test : Envoi de /count et check du nombre reçu (`{"pingCount":"1"}`)
- 3ème test : Envoi de 3 pings et check du nombre reçu (`{"pingCount":"3"}`)

Contacter antoine.nguyen3@epsi.fr ou nicolas.demoulin1@epsi.fr pour tout renseignement.
